# elmo

ELMO v 0.99 - Error Log Monitor and Organizer

ELMO is your guy if you need to monitor a text file for changes.
Run him in a terminal, he will then inform you of changes made.
He can be instructed to force redirection to /dev/null if needed.