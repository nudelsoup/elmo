#!/bin/bash

# beerware license. do what you want with this script, just keep this notice.
# if this script brings you value, buy me a beer or whatever. or don't.
# written by nudelsoup aka thomas.rosted@gmail.com
#
# for description, usage, examples and options, use --help

SHRTNAME='elmo'
LONGNAME='error log monitor and organizer'
VERSION='0.99'

# value listing
printdata () {
    clear
    echo; echo -e "  script started: \t $scriptstartstr"
    echo -e "  current time: \t $currenttimestr"
    if (( $loopduration > 1 )); then echo -e "  refresh rate: \t\t\t $loopduration""s"; fi
    echo; echo -e "  stats for file $targetrealpath"
    echo -e "    original total linecount: \t\t $originalcount"
    echo -e "    new lines this session:   \t\t $sessioncount"
    if $haspanicked; then
        echo -e "    linecount after last panic:    \t $postpanictotal"
        echo -e "    new lines since last panic:    \t $postpaniccount"
    fi
    echo -e "    current total linecount:  \t\t $totalcount"
    echo -e "    recent new lines: \t\t\t $recentcount"
    if $panicdetectionenabled; then
        echo; echo "  panic detection enabled"
        echo -e "    same-minute panic threshold:   \t $panicthreshold"
        echo -e "    panic mode shutdown duration:  \t $shutdownduration"
        echo -e "    total panic mode redirections: \t $paniccounter"
    fi
    itemindex=0
    for item in "${watchlist[@]}"; do
        echo; echo -e "  data for watched item: \t ${watchlist[$itemindex]}"
        echo -e "    occurrences at script start: \t ${itemcountoriginal[$itemindex]}"
        echo -e "    new occurrences this session: \t ${itemcountsession[$itemindex]}"
        if $haspanicked; then
            echo -e "    occurrences after last panic: \t ${itemtotalpostpanic[$itemindex]}"
            echo -e "    new occurrences after panic:  \t ${itemcountpostpanic[$itemindex]}"
        fi
        echo -e "    total current occurrences: \t\t ${itemcounttotal[$itemindex]}"
        echo -e "    recent occurrences: \t\t ${itemcountrecent[$itemindex]}"
        (( itemindex++ ))
    done
    echo; echo "  --------first-5-lines-in-file--------"
    for line in "${outputheadpart[@]}"; do echo "    $line"; done
    echo; echo "  --------last-30-lines-in-file--------"
    for line in "${outputtailpart[@]}"; do echo "    $line"; done
    echo; echo "  --------data-collection-end--------"
}

# updates available info about the shutdown. can probably be expanded.
printpanicinfo () {
    clear
    echo -e "  script started: \t $scriptstartstr"
    echo -e "  current time: \t $currenttimestr"
    echo; echo "  panic triggered, $targetrealpath redirected to /dev/null"; echo
    echo -e "    same-minute occurrences needed:\t $panicthreshold"
    echo -e "    panic mode shutdown duration:  \t $shutdownduration"
    echo -e "    total panic mode redirections: \t $paniccounter"; echo
    if (( $shutdownremaining < 2 )); then echo "    resuming normal operations..."
    else echo -e "    normal operations countdown:   \t $shutdownremaining seconds"; fi
}

# general file data - updates absolute data, initiates itemdata and detects tampering
parsenewdata () {
    if $panictriggered; then return 1; fi
    mapfile -t newlines < <(tail -n +$(( $totalcount + 1 )) "$targetrealpath")
    local tailsample=$(tail -n +$(( $totalcount - 1 )) "$targetrealpath")
    newlinecount=${#newlines[@]}
    if (( $newlinecount )); then
        (( totalcount += "${#newlines[@]}" ))
        (( sessioncount += "${#newlines[@]}" ))
        if $haspanicked; then (( postpaniccount += "${#newlines[@]}" )); fi
        mapfile -t outputtailpart < <(tail -n 30 "$targetrealpath")
    elif (( ${#tailsample} == 0 )) && (( $(cat "$targetrealpath" | wc -l) < $totalcount )); then 
        tamperresponse=true
        initdataset
        return 1
    fi
    recenthistoryposition=$(( ($cycleposition - $datapointsprminute) / ${#watchlist[@]} ))
    itemcounthistory[$recenthistoryposition]=$newlinecount
    recentcount=$(gethistorytotal)
    itemindex=0
    for item in "${watchlist[@]}"; do
        if ! parseitemdata; then return 1; fi
        (( itemindex++ ))
    done; return 0
}

# helper function 0 - updates data for current search term and triggers panic mode if enabled / threshold exceeded
parseitemdata () {
    if $panictriggered; then return 1; fi
    local itemcountfresh=0
    if (( ${#newlines} )); then
        itemcountfresh=$(getitemmatches)
        (( itemcounttotal[$itemindex] += itemcountfresh ))
        (( itemcountsession[$itemindex] += itemcountfresh ))
        if $haspanicked; then (( itemcountpostpanic[$itemindex] += itemcountfresh )); fi
    fi
    itemcounthistory[$cycleposition]=$itemcountfresh
    itemcountrecent[$itemindex]=$(getitemhistory)
    (( cycleposition++ ))
    if (( $cycleposition == $totalreservedslots )); then cycleposition=$datapointsprminute; fi
    if $panicdetectionenabled && (( $panicthreshold < ${itemcountrecent[$itemindex]} )); then
        panictriggered=true
        return 1
    fi; return 0
}

# helper function 1 - tests each newline for current search term, returns #matches
getitemmatches () {
    confirmmatch () {
        for word in ${watchlist[$itemindex]}; do
            echo "${newlines[$searchtermindex]}" | grep -e "$word" &>/dev/null 2>&1
            if [[ $? != 0 ]]; then return 1; fi
        done; return 0
    }
    local searchtermmatches=0 searchtermindex=0
    while (( $searchtermindex < "${#newlines[@]}" )); do
        if confirmmatch; then (( searchtermmatches++ )); fi
        (( searchtermindex++ ))
    done; echo $searchtermmatches
}

# helper function 2a - returns absolute recent newlines - could probably be merged with 2b
gethistorytotal () {
    local historyindex=0 historyresult=0
    while (( $historyindex < $datapointsprminute )); do
        (( historyresult += itemcounthistory[$historyindex] ))
        (( historyindex++ ))
    done; echo $historyresult
}

# helper function 2b - returns recent newlines matches for current search term
getitemhistory () {
    local historyindex=$(( $itemindex + $datapointsprminute ))
    local watcheditems=${#watchlist[@]} historyresult=0
    while (( $historyindex < $totalreservedslots )); do
        (( historyresult += itemcounthistory[$historyindex] ))
        (( historyindex += watcheditems ))
    done; echo $historyresult
}

# shut down action, monitoring while shut down, and resume normal operations
handlepanic () {
    local timenow=$(date +'%s')
    if $panicdetectionenabled; then
        mv "$targetrealpath" "$targetrealpath"".panic"
        ln -sf /dev/null "$targetrealpath"
        panicdetectionenabled=false
        haspanicked=true
        shutdownuntil=$(( $timenow + $shutdownduration ))
        shutdownremaining=$shutdownduration
        return 1
    fi
    shutdownremaining=$(( $shutdownuntil - $timenow ))
    if (( $shutdownremaining < 1 )); then
        shutdownremaining=0
        initdataset
        rm "$targetrealpath"
        mv "$targetrealpath"".panic" "$targetrealpath"
        panictriggered=false
        panicdetectionenabled=true
    fi; return 0
}

# self-explanatory
backupfiletarget () {
    cp "$targetrealpath" "$targetrealpath"".bak"
}

# debug function - append false positives to file
injectfalsepositives () {
    local i=0
    echo; echo "injection activated, repeating $debug times"
    while (( $i < ${#watchlist[@]} )); do
        local j=0
        echo "inserting ${watchlist[$i]} into $targetrealpath"
        while (( $j < $doinjections )); do
            echo "${watchlist[$i]}" >> "$targetrealpath"; echo -n "."
            (( j++ ))
        done; echo
        (( i++ ))
    done; echo "finished injecting false positives, exiting"; echo
}

printversion () {
    echo "$SHRTNAME $VERSION - $LONGNAME"
}

# TODO proper columns, columnwidth, formatting - note: irrelevant but nice
printhelp () {
    helpstring="
  description:

    scans a target text file for given search terms. can be instructed to redirect the
    target file to /dev/null. useful for monitoring large, dynamic files like error logs.

  usage:        bash /path/to/elmo.sh [options]

  options:

    --target=[path/to/file]     the target file to scan. default is $HOME/.steam/error.log

    --watch=[search_term]       what to scan newlines for. can be used multiple times, each
                                will add another search term to monitor for. if this option is
                                not supplied, default search terms will be used.

    --refresh=[1..60]           sets the refresh rate to [int] (default=1). this rate
                                determines how often newlines in the targetfile are processed.
                                must be an integer between 1 and 60.

    --panic-trigger=[int]       enables panic detection and sets the threshold for panic
                                shutdown of the filetarget. if this threshold is exceeded by
                                any search term, the target file will be redirected to
                                /dev/null. panic detection is disabled by default.

    --shutdown-duration=[int]   sets the shutdown duration (in seconds, default=300). this value
                                is only used when panic detection is enabled. exceeding the panic
                                threshold will redirect to /dev/null for this amount of seconds.
                                after the shutdown duration, normal operations will be resumed.

    --do-injections=[int]       appends [int] seach terms to the target file, then exits.
                                supplying the [int] is optional, default is 5. useful for testing
                                detection rate while the main script is running in another shell.

    --backup                    copies the filetarget to a new file /path/to/target.file.bak
                                then exits. useful if you have a nice, juicy target.file.

    --help                      prints this help text, then exits.

    --version                   prints version information, then exits.

  examples:

    /path/to/elmo.sh --version --backup --help
        - prints version information, helptext, and makes a backup of the default filetarget.

    /path/to/elmo.sh --refresh=60
        - monitors the default filetarget for new instances of the default search term(s),
          with a refresh rate of 1 minute (slowest).

    /path/to/elmo.sh --refresh=6 --panic-trigger=50
        - sets the refresh rate to every 6 seconds and activates redirection to /dev/null if any
          default search term(s) are added to the default filetarget 50 times in the past minute.

    /path/to/elmo.sh --target='/path/to/random.file' --watch='the cake is a lie'
        - scans random.file for new occurences of the phrase 'the cake is a lie' using default
          settings.
  notes:

    slow refresh rates result in larger bulks of data processing. unnecessary operations have been
    cut where possible, and it is recommended to refresh often.

    panic detection is based on the most possible data from the past minute, awkward refresh rates
    will affect this, e.g --refresh=7 will truncate the collection period to 56 seconds.

    this script is not properly tested. edit the script at your leisure, but pls provide feedback.
    use with caution. 
"
    echo "$helpstring"
}

# initialize values - removes the need for a panic detection grace period
initdataset () {
    local filetarget="$targetrealpath"
    if ! $haspanicked && ! $tamperresponse; then echo "initializing dataset..."
    elif $tamperresponse; then echo "tampering detected, recalculating..."; tamperresponse=false; sleep 1
    else filetarget+='.panic'; fi
    mapfile -t newlines < <(cat "$filetarget")
    if $haspanicked; then
        postpanictotal=${#newlines[@]}
        totalcount=$postpanictotal
        (( paniccounter++ ))
        sessioncount=$(( $totalcount - $originalcount ))
        if (( $sessioncount < 0 )); then sessioncount=0; fi
    else
        originalcount=${#newlines[@]}
        totalcount=$originalcount
        postpanictotal=0
        sessioncount=0
        paniccounter=0
    fi
    postpaniccount=0
    recentcount=0
    itemindex=0
    cycleposition=$datapointsprminute
    mapfile -t outputheadpart < <(head -n 5 "$filetarget")
    mapfile -t outputtailpart < <(tail -n 30 "$filetarget")
    for item in "${watchlist[@]}"; do
        if $haspanicked; then
            itemtotalpostpanic[$itemindex]=$(getitemmatches)
            itemcounttotal[$itemindex]=${itemtotalpostpanic[$itemindex]}
            itemcountsession[$itemindex]=$(( ${itemcounttotal[$itemindex]} - ${itemcountoriginal[$itemindex]} ))
            if (( ${itemcountsession[$itemindex]} < 1 )); then itemcountsession[$itemindex]=0; fi
        else
            itemcountoriginal[$itemindex]=$(getitemmatches)
            itemcounttotal[$itemindex]=${itemcountoriginal[$itemindex]}
            itemtotalpostpanic[$itemindex]=0
            itemcountsession[$itemindex]=0
        fi
        itemcountpostpanic[$itemindex]=0
        itemcountrecent[$itemindex]=0
        (( itemindex++ ))
        (( cycleposition++ ))
    done; inititemhistory
}

# populates the history list with zeroes
inititemhistory () {
    local historyindex=0
    while (( $historyindex < $totalreservedslots )); do
        itemcounthistory[$historyindex]=0
        (( historyindex++ ))
    done
}

# TODO add profiles - migrate the .steam/error.log defaults to profile
# TODO make profiles using cmdline args
# TODO filter args based on type - reject non-integer args where integers required
# TODO add function to clean up error log by deleting lines containing watched items
# TODO add option autocleanup while running
# TODO add options to customize/hide head/tail parts
parseargs () {
    scriptstart=$(date +'%s')
    scriptstartstr=$(date +'%F%t%H:%M:%S')
    doinjections=0
    target="$HOME/.steam/error.log" # TODO move to profile
    loopduration=1
    dobackup=false
    dohelp=false
    doversion=false
    exitcondition=false
    panicdetectionenabled=true
    panicthreshold=100
    shutdownduration=300
    panictriggered=false
    haspanicked=false
    tamperresponse=false
    watchlist=()
    for arg in "$@"; do
        case $arg in
            --target=* )
                targetrealpath="${arg#*=}"; shift
                ;;
            --watch=* )
                watchlist+=("${arg#*=}"); shift
                ;;
            --refresh=* )
                loopduration=("${arg#*=}"); shift
                ;;
            --panic-trigger=* )
                panicthreshold=("${arg#*=}")
                if (( $panicthreshold < 1 )); then panicdetectionenabled=false; fi; shift
                ;;
            --shutdown-duration=* )
                shutdownduration=("${arg#*=}"); shift
                ;;
            --do-inject=* )
                exitcondition=true; doinjections=("${arg#*=}"); shift
                ;;
            --backup )
                exitcondition=true; dobackup=true; shift
                ;;
            --help )
                exitcondition=true; dohelp=true; shift
                ;;
            --version )
                exitcondition=true; doversion=true; shift
                ;;
            * )
                shift
                ;;
        esac
    done
}

initscript () {
    # if custom refresh rate, fix if invalid 
    if (( $loopduration > 60 )); then loopduration=60; fi
    if (( $loopduration < 1 )); then loopduration=1; fi
    #  test if redirection - try fix else create target
    targetrealpath=$(realpath -s "$target")
    if (( $doinjections == 0 )) && [ $(realpath "$target") = "/dev/null" ]; then
        if [ -f "$targetrealpath"".bak" ]; then
            cp --remove-destination "$targetrealpath"".bak" "$targetrealpath"
        else rm "$targetrealpath"; touch "$targetrealpath"
    fi; fi
    # TODO move to default profile
    # add strings to watchlist if none given
    if (( ${#watchlist[@]} == 0 )); then
        watchlist+=('keyboard_code_conversion_x')
        watchlist+=('type unknown not a basic type')
    fi
    # early exit if given args --backup --help --version --do-injections
    if $exitcondition; then
        if $dobackup; then backupfiletarget; fi
        if $doversion; then printversion; fi
        if $dohelp; then printhelp; fi
        if (( $doinjections )); then injectfalsepositives; fi
        exit 0
    fi
    datapointsprminute=$(( (60/loopduration)+(((60%(60/loopduration))*2)/loopduration) ))
    totalreservedslots=$(( datapointsprminute + ("${#watchlist[@]}"* datapointsprminute) ))
}

# here we go
parseargs "${@}"
initscript
initdataset 
while :; do
    currenttimestr=$(date +'%F%t%H:%M:%S')
    if parsenewdata; then printdata
    elif $panictriggered; then handlepanic; printpanicinfo; fi
    sleep $loopduration
done
exit

